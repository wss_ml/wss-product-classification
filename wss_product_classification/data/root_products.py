import pandas as pd
import numpy as np
import pickle as pkl
import os
from urllib import request
from time import time

test_url = "https://gitlab.com/subgraphiso/datasets/raw/master/root_products/product-test-150k.csv"
train_url = "https://gitlab.com/subgraphiso/datasets/raw/master/root_products/product-train-150k.csv"
vector_url = "https://gitlab.com/subgraphiso/datasets/raw/master/root_products/3grams_glove.pkl"

def maybe_download(fname, url, fpath):
  """Check if the file exists, if not download the file

  Arguments:
    fname: File name with extension (e.g. product.csv)
    url: Direct link to the file
    fpath: Path to the download folder (e.g. ~/.wss/root_products)

  Returns:
    None
  """
  if not os.path.exists(fpath):
    print("Creating data cache at {}".format(fpath))
    os.makedirs(fpath, exist_ok=True)
  if not os.path.exists(os.path.join(fpath, fname)):
    print("Downloading data to {}".format(fpath))
    request.urlretrieve(url, os.path.join(fpath, fname))
  else:
    print("Dataset {} exists. Skipping download.".format(fname))

def load_data():
  """Load the root products dataset.

  Returns:
    (x_train, y_train), (x_test, y_test)
  """
  datadir = os.path.join(os.path.expanduser('~/.wss'), 'root_products')
  vectordir = os.path.join(os.path.expanduser('~/.wss'), 'vectors')
  test_file = os.path.join(datadir, "product-test-150k.csv")
  train_file = os.path.join(datadir, "product-train-150k.csv")
  vector_file = os.path.join(vectordir, "3grams_glove.pkl")

  maybe_download("product-test-150k.csv", test_url, datadir)
  maybe_download("product-train-150k.csv", train_url, datadir)
  maybe_download("3grams_glove.pkl", vector_url, vectordir)

  with open(vector_file, "rb") as f: 
    dictionary = pkl.load(f)

  train_names, train_labels, train_tokens = to_token(train_file, dictionary)
  test_names, test_labels, _ = to_token(test_file, dictionary, False)

  train_tk = tokenize(train_names, train_tokens)
  test_tk = tokenize(test_names, train_tokens)

  fixed_size_sequence(train_tk)
  fixed_size_sequence(test_tk)

  return (np.array(train_tk), np.array(train_labels)),\
         (np.array(test_tk), np.array(test_labels)),\
         train_tokens, dictionary

def to_token(fpath, dictionary, build_token=True):
  """Read the csv file and split the product name following words
  in dictionary and build a tokenizer. Currenty this function is
  hard-coded to handle at most 3-grams words.

  Arguments:
    fpath: Path to the target data file
    dictionary: Dictionary mapping n-grams words to vectors
    built_token: Flag to decide if a token builder is needed 

  Returns:
    names_splitted: List of product names splitted by the dictionary
    
  """
  token = None
  if build_token:
    token = dict()
  datasets = pd.read_csv(fpath)
  names_splitted = []
  labels = []
  for _, r in datasets.iterrows():
    names_splitted.append(_split_by_dict(r.Name, dictionary, token))
    labels.append(r.ClassId)
  return names_splitted, labels, token

def tokenize(word_list, token):
  return [[token[i] for i in j if i in token] for j in word_list]

def fixed_size_sequence(token_lists, length=10):
  """Pad zeros or trim each token sequence in the token list.
  This function modifies `token_lists` in place.
  """
  for i in range(len(token_lists)):
    if len(token_lists[i]) < length:
      token_lists[i].extend([0]*(length-len(token_lists[i])))
    elif len(token_lists[i]) > length:
      token_lists[i] = token_lists[i][:length] 

def _split_by_dict(name, dictionary, token):
  words = name.split()
  i = 0
  buf = []
  while i < len(words):
    w3 = " ".join(words[i:i+3])
    w2 = " ".join(words[i:i+2])
    w1 = words[i]
    if w3 in dictionary:
      _build_token(w3, token)
      buf.append(w3)
      i += 3
    elif w2 in dictionary:
      _build_token(w2, token)
      buf.append(w2)
      i += 2
    else:
      _build_token(w1, token)
      buf.append(w1)
      i += 1
  return buf

def _build_token(word, token):
  if token is None:
    pass
  elif not word in token:
    token[word] = len(token) + 1
