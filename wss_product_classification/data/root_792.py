import pandas as pd
import numpy as np
import pickle as pkl
import os
from urllib import request
from pyvi import ViTokenizer

test_url = "https://gitlab.com/subgraphiso/datasets/raw/master/root_products_792/test_root_products_792.csv"
train_url = "https://gitlab.com/subgraphiso/datasets/raw/master/root_products_792/train_root_products_792.csv"
tokens_url = "https://gitlab.com/subgraphiso/datasets/raw/master/root_products_792/tokens_60k.txt"

def maybe_download(fname, url, fpath):
  """Check if the file exists, if not download the file

  Arguments:
    fname: File name with extension (e.g. product.csv)
    url: Direct link to the file
    fpath: Path to the download folder (e.g. ~/.wss/root_products)

  Returns:
    None
  """
  if not os.path.exists(fpath):
    print("Creating data cache at {}".format(fpath))
    os.makedirs(fpath, exist_ok=True)
  if not os.path.exists(os.path.join(fpath, fname)):
    print("Downloading data to {}".format(fpath))
    request.urlretrieve(url, os.path.join(fpath, fname))
  else:
    print("Dataset {} exists. Skipping download.".format(fname))

def load_data():
  """Load the root products dataset.

  Returns:
    (x_train, y_train), (x_test, y_test)
  """
  datadir = os.path.join(os.path.expanduser('~/.wss'), 'root_products')
  test_file = os.path.join(datadir, "test_root_products_792.csv")
  train_file = os.path.join(datadir, "train_root_products_792.csv")
  token_file = os.path.join(datadir, "tokens_60k.txt")

  maybe_download("test_root_products_792.csv", test_url, datadir)
  maybe_download("train_root_products_792.csv", train_url, datadir)
  maybe_download("tokens_60k.txt", tokens_url, datadir)

  train_names, train_labels, _ = to_token(train_file, build_token=False)
  test_names, test_labels, _ = to_token(test_file, build_token=False)

  train_tokens = dict()
  with open(token_file, "r") as f:
    for i, t in enumerate(f.readlines()):
      train_tokens[t.strip()] = i + 1

  train_tk = tokenize(train_names, train_tokens)
  test_tk = tokenize(test_names, train_tokens)

  fixed_size_sequence(train_tk)
  fixed_size_sequence(test_tk)

  return (np.array(train_tk), np.array(train_labels)),\
         (np.array(test_tk), np.array(test_labels)),\
         train_tokens

def to_token(fpath, build_token=True):
  """Read the csv file and split the product name using ViTokenizer.

  Arguments:
    fpath: Path to the target data file
    built_token: Flag to decide if a token builder is needed 

  Returns:
    names_splitted: List of product names splitted by the dictionary
    
  """
  token = None
  if build_token:
    token = dict()
  datasets = pd.read_csv(fpath)
  names_splitted = []
  labels = []
  for _, r in datasets.iterrows():
    names_splitted.append(_split_by_dict(r.Name, token))
    labels.append(r.ClassId)
  return names_splitted, labels, token

def tokenize(word_list, token):
  return [[token[i] for i in j if i in token] for j in word_list]

def fixed_size_sequence(token_lists, length=10):
  """Pad zeros or trim each token sequence in the token list.
  This function modifies `token_lists` in place.
  """
  for i in range(len(token_lists)):
    if len(token_lists[i]) < length:
      token_lists[i].extend([0]*(length-len(token_lists[i])))
    elif len(token_lists[i]) > length:
      token_lists[i] = token_lists[i][:length] 

def _split_by_dict(name, token):
  """Split a string by ViTokenizer and build tokens.
  Each string is truncated by a fixed length of 10."""
  name_tk = ViTokenizer.tokenize(name).split()[:10]
  for t in name_tk:
    _build_token(t, token)
  return name_tk
  
def _build_token(word, token):
  if token is None:
    pass
  elif not word in token:
    token[word] = len(token) + 1
