import numpy as np
from keras import backend as K
from ..data import root_792
from ..models import cnn

"""Experiments:
 - conv: Adjust the number of convolutional layers
"""

def run_expr_conv_units(data=None):
  if data is None:
    data = root_792.load_data()
  print("Testing the effect of the number of unit in a conv layer...")
  for i in [32, 64, 128, 256, 512, 1024, 2048, 4096]:
    _expr_conv_units(data, i)

def run_expr_conv_layers(data=None):
  if data is None:
    data = root_792.load_data()
  print("Testing the effect of the number of conv unit layers...")
  for i in [[32, 32], [32, 32, 32], [32, 32, 32, 32], 
            [32, 64], [32, 64, 128], [32, 64, 128, 256],
            [32, 32, 32, 64, 64, 64], [32, 32, 32, 64, 64, 64, 128, 128, 128]]:
    _expr_conv_layers(data, i)

def _expr_conv_units(data=None, num_units=128):
  if data is None:
    train, test, tk = root_792.load_data()
  else:
    train, test, tk = data
  
  # Other hyperparams
  batch_size = 128
  e = 20
  init_lr = 0.01 # Adagrad
  
  model1 = cnn.vanila_cnn(len(tk), emb_init=None,
                          conv1d_filters=[num_units],
                          kernel_sizes=[2],
                          denses=[1024, 1157], init_lr=init_lr)
  print(model1.summary()) 
  model1.fit(*train, validation_data=test, batch_size=batch_size, epochs=e, verbose=2)
  val_score = model1.evaluate(*test)
  model1.save("cnn_conv_units_{}_test_{}.keras".format(val_score[1], num_units))
  del model1
  K.clear_session()

def _expr_conv_layers(data=None, layers=[32, 64]):
  if data is None:
    train, test, tk = root_792.load_data()
  else:
    train, test, tk = data
  
  # Other hyperparams
  batch_size = 128
  e = 20
  init_lr = 0.01 # Adagrad
  
  model1 = cnn.vanila_cnn(len(tk), emb_init=None,
                          conv1d_filters=layers,
                          kernel_sizes=[2]*len(layers),
                          denses=[1024, 1157], init_lr=init_lr)
  print(model1.summary()) 
  model1.fit(*train, validation_data=test, batch_size=batch_size, epochs=e, verbose=2)
  val_score = model1.evaluate(*test)
  model1.save("cnn_conv_layers_test_{}.keras".format(val_score[1]))
  del model1
  K.clear_session()
