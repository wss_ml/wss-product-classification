import numpy as np
from keras import backend as K
from ..data import root_792
from ..models import cnn

"""Experiments:
 - dense: Adjust the number of units and the number of dense layers
"""

def run_expr_dense_units(data=None):
  if data is None:
    data = root_792.load_data()
  print("Testing the effect of the number of dense unit layer...")
  for i in [32, 64, 128, 256, 512, 1024, 2048, 4096]:
    _expr_dense_units(data, i)

def _expr_dense_units(data=None, num_units=128):
  if data is None:
    train, test, tk = root_792.load_data()
  else:
    train, test, tk = data
  
  # Other hyperparams
  batch_size = 128
  e = 20
  init_lr = 0.01 # Adagrad
  
  model1 = cnn.vanila_cnn(len(tk), emb_init=None,
                          conv1d_filters=[256],
                          kernel_sizes=[2],
                          denses=[num_units, 1157], init_lr=init_lr)
  print(model1.summary()) 
  model1.fit(*train, validation_data=test, batch_size=batch_size, epochs=e, verbose=2)
  val_score = model1.evaluate(*test)
  model1.save("cnn_dense_units_{}_test_{}.keras".format(val_score[1], num_units))
  del model1
  K.clear_session()
