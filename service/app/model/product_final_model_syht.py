# Class Model by syht
import time
import pandas as pd
import numpy as np

from pyvi.pyvi import ViTokenizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC

class ProductData(object):
    def __init__(self, seg_word=True):
        self.seg_word = seg_word
    
    def proccess_data(self, X):
        # process data
        X_ = []
        X_seg = []
        X_tok = []
        
        for doc in X:
            doc_pro = doc.lower()
            if self.seg_word:
                doc_pro = ViTokenizer.tokenize(doc_pro)
                X_seg.append(doc_pro)
            X_.append(doc_pro)
            X_tok.append(doc_pro.split())
            
        return X_, X_seg, X_tok
    
    def prepare(self, X, y):
        self.X = X
        self.y = y
        self.X_, self.X_seg, self.X_tok = self.proccess_data(X)
        return self.X_
        
    # vectorize by tfidf
    def vectorizer(self, test_size=0, ngram_range=(1,2), max_df=0.25):
        self.tfidf = TfidfVectorizer(ngram_range=ngram_range, max_df=max_df)
        
        if test_size > 0:
            # split dataset to train set and test set
            self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.X_, self.y, test_size = test_size, random_state = 0)
        else:
            self.X_train, self.X_test, self.y_train, self.y_test = (self.X_, [], self.y, [])
        
        # fit and transform by tfidf
        self.X_train = self.tfidf.fit_transform(self.X_train)
        self.X_test = self.tfidf.transform(self.X_test) if test_size > 0 else []
        
        if test_size > 0:
            return self.X_train, self.X_test, self.y_train, self.y_test  
        else:
            return self.X_train, self.y_train
        
    def vectorize_transform(self, X):
        X, _, _ = self.proccess_data(X)
        X = self.tfidf.transform(X)
        
        return X

    
class LinearSVC_proba(LinearSVC):
    def __platt_func(self,x):
        return 1/(1+np.exp(-x))

    def predict_proba(self, X):
        f = np.vectorize(self.__platt_func)
        raw_predictions = self.decision_function(X)
        platt_predictions = f(raw_predictions)
        probs = platt_predictions / platt_predictions.sum(axis=1)[:, None]
        return probs
    
    
class MyModel(object):    
    def __init__(self):
        self.name = 'syht-final-model'
        self.model = LinearSVC_proba(C=1e3)
        self.data_process = ProductData()
        
    def fit(self, X, y):
        start = time.time()
        
        # proccessing data
        self.data_process.prepare(X, y)
        X_train, y_train = self.data_process.vectorizer()
        
        # training        
        self.model.fit(X_train, y_train)
        
        end = time.time()
        print('runtime: ', (end - start))
        
    def predict(self, X):
        # proccessing data
        X_pred = self.data_process.vectorize_transform(X)
        
        # predicting
        return self.model.predict(X_pred)
        
    def predict_proba(self, X):
        # proccessing data
        X_pred = self.data_process.vectorize_transform(X)
        
        # predicting
        return self.model.predict_proba(X_pred)