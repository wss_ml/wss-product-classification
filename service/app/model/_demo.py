from ngramRun import *
from nn1200 import *
from product_final_model_syht import *
import pickle
import keras
from sklearn.externals import joblib
from time import time
from collections import Counter

start = time()

class CombinedModel:
    def __init__(self):
        self._name = "Combined model"
        with open("bin/ngram-model-100k-2.pkl", "rb") as f:
            self.m1 = pickle.load(f)
        self.m2 = joblib.load("bin/product_model_syht.joblib")
        k3 = keras.models.load_model("bin/nn_1200.keras") 
        with open("bin/wiki.vi.pkl", "rb") as f:
            self.wkvi = pickle.load(f)
        with open("bin/cmap.pkl", "rb") as f:
            self.cmap = pickle.load(f)
        with open("bin/rmap.pkl", "rb") as f:
            self.rmap = pickle.load(f)
        self.m3 = NNModel(core=k3, vocab=self.wkvi, label_map=self.cmap, reverse_map=self.rmap)

    def predict(self, X):
        p1 = self.m1.predict(X)
        p2 = self.m2.predict(X)
        p3 = self.m3.predict(X)
        results = []
        for i, j, k in zip(p1, p2, p3):
            i = int(i)  # hot fix ngram
            c = Counter([i, j, k])
            p = c.most_common(1)[0][0]
            results.append((p, 3-len(c)))
        return results

    def predict_name(self, X):
        results = self.predict(X)
        named_results = []
        for p, r in results:
            named_results.append((self.to_name(p), r))
        return named_results


    def to_name(self, p):
        if p in self.rmap:
            return self.rmap[p]
        else:
            return (-1, "unknown")
