import operator
import re
import csv
import io
import pickle
import sklearn.metrics


class NgramModel:

    dictWithLabel = {}

    def fit(self, x_train, y_train, ngram=6):
        self.dictWithLabel = {}
        for i in range(1, ngram + 1):
            self.ngramLabelBuilder(x_train, y_train, i)

    def predict(self, x_tests):
        result = []
        for x_test in x_tests:
            predict = self.scoringNgram(x_test, self.dictWithLabel)

            if len(list(predict.keys())) > 0:
                ranking = predict[list(predict.keys())[0]]
                predictLabel = sorted(
                    ranking.items(), key=operator.itemgetter(1), reverse=True)[0][0]
                result.append(predictLabel)
            else:
                result.append(-1)

        return result

    def ngramLabelBuilder(self, x_train, y_train, ngram):
        count = -1
        for row in x_train:
            count += 1
            label = y_train[count]

            ngramRow = self.ngramInString(row, ngram)
            for k in ngramRow:
                if self.dictWithLabel.get(k):

                    if self.dictWithLabel[k].get(label):
                        self.dictWithLabel[k][label] += 1
                    else:
                        self.dictWithLabel[k][label] = 1
                else:
                    self.dictWithLabel[k] = {label: 1}

    def ngramInString(self, sentence, ngram):
        return self.ngramInArray(sentence.split(), ngram)

    def ngramInArray(self, arrSentence, ngram):
        ngrams = {}
        for j in range(0, len(arrSentence) - ngram + 1):
            word = ' '.join(arrSentence[j: j + ngram])
            if ngrams.get(word):
                ngrams[word] += 1
            else:
                ngrams[word] = 1
        return ngrams

    def longestCharInNgram(self, sentence, dict):
        allGrams = []
        for i in range(1, len(sentence.split()) + 1):
            allGrams.extend(
                list(self.ngramInString(sentence.lower(), i).keys()))

        allGrams.sort()

        longestLength = 0
        result = {}
        for gram in allGrams:
            if len(gram.split()) > longestLength and dict.get(gram):
                result = {gram: dict.get(gram)}
                longestLength = len(gram.split())

        return result

    def scoringNgram(self, sentence, dict):
        allGrams = []
        for i in range(1, len(sentence.split()) + 1):
            allGrams.extend(
                list(self.ngramInString(sentence.lower(), i).keys()))

        allGrams.sort()

        maxScore = 0
        result = {}

        for gram in allGrams:
            if dict.get(gram) and self.score(gram, dict.get(gram)) > maxScore and dict.get(gram):
                result = {gram: dict.get(gram)}
                maxScore = self.score(gram, dict.get(gram))

        return result

    def score(self, gram, freq):
        N = len(gram.split())
        # TODO: need implement so if it in one parent group will have high score
        Favg = sum(freq.values()) / (len(freq.values()) ** 2)

        try:
            Favg = float(Favg)
        except ValueError:
            Favg = 0
        return N ** 3 * Favg

    def test(self, trainFile, testFile):
        rows = []

        print("-----load train file started-----")
        with io.open(trainFile, 'r') as csvfile:
            rows = csv.reader(csvfile, delimiter=',', quotechar='"')
            rows = list(rows)

        x_trains = [row[1] for row in rows[1:]]
        y_trains = [row[2] for row in rows[1:]]
        print("-----load train file  ended-----")

        print("-----train started-----")
        self.fit(x_trains, y_trains)
        print("-----train ended-----")

        print("-----load test file started-----")
        with io.open(testFile, 'r') as csvfile:
            rows = csv.reader(csvfile, delimiter=',', quotechar='"')
            rows = list(rows)

        x_tests = [row[1] for row in rows[1:]]
        y_tests = [row[2] for row in rows[1:]]
        print("-----load test file ended-----")

        print("-----predict started-----")
        y_predicts = self.predict(x_tests)
        print("-----predict ended-----")

        print("test result")
        print(sklearn.metrics.accuracy_score(y_tests, y_predicts))


def test(trainFile, testFile):
    model = NgramModel()
    rows = []

    print("-----load train file started-----")
    with io.open(trainFile, 'r') as csvfile:
        rows = csv.reader(csvfile, delimiter=',', quotechar='"')
        rows = list(rows)

    x_trains = [row[1] for row in rows[1:]]
    y_trains = [row[3] for row in rows[1:]]
    print("-----load train file  ended-----")

    print("-----train started-----")
    model.fit(x_trains, y_trains)
    print("-----train ended-----")

    print("-----save model started-----")
    with open("ngram-100k", 'wb') as pickle_file:
        pickle.dump(model, pickle_file)
    print("-----save model end-----")

    print("-----load test file started-----")
    with io.open(testFile, 'r') as csvfile:
        rows = csv.reader(csvfile, delimiter=',', quotechar='"')
        rows = list(rows)

    x_tests = [row[1] for row in rows[1:]]
    y_tests = [row[3] for row in rows[1:]]
    print("-----load test file ended-----")

    print("-----predict started-----")
    y_predicts = model.predict(x_tests)
    print("-----predict ended-----")

    print("test result")
    print(sklearn.metrics.accuracy_score(y_tests, y_predicts))


if __name__ == '__main__':
    test("./data/product-train-100k.csv", "./data/product-test-100k.csv")
