import os
import sys
sys.path.append(os.getcwd() + '/model')
sys.path.append(os.getcwd() + '/ultils')

import _thread
from flask import Flask, request, jsonify, abort
from flasgger import Swagger
from model._demo import *
import ultils.constants as ct
import ultils.helper as hp

app = Flask(__name__)
app.debug = True
Swagger(app)

model = CombinedModel()
model.predict(['test api']) # this trick resolves a problem with tensorflow running on service


@app.route('/', methods=['GET'])
def hello():    
    return 'Hello!'


@app.route('/cat', methods=['POST'])
def predict():
    """
    Predict product category API
    Call this api passing a list of product's names and get back the categories
    ---
    tags:
      - Category Prediction
    parameters:
      - name: body
        in: body
        required: true
        description: list of product's names
        schema:
            id: products
            type: object
            example: '{"products" : ["product name 1", "product name 2"]}'
    responses:
      200:
        description: Categories
        schema:
          id: category
          properties:
            results:
              type: array
              items:
                  schema:
                    description: product with it's category
                    properties:
                        product_name:
                            type: string
                            description: input product name
                        cat_id: 
                            type: string
                            description: category id
                        cat_name: 
                            type: string
                            description: category name
                        score:
                            type: string
                            description: prediction's score, higher is better
            version:
              type: string

    """
    print('Predict ====================================================== ')
    # get product name
    products = request.get_json('products')
    products = products['products']
    
    print('input: ', products)
    
    # predict category    
    cate_pred = model.predict_name(products)
    categories = []
    bad_prediction = []
    for i in range(len(products)):
        categories.append({
            'product_name' : products[i],
            'cat_id' : str(cate_pred[i][0][0]),
            'cat_name' : cate_pred[i][0][1],
            'score' : str(cate_pred[i][1])
        })
    
        # log low certaincy product prediction
        if cate_pred[i][1] == 0:
            bad_prediction.append([products[i], str(cate_pred[i][0][0]), cate_pred[i][0][1]])
        
    if len(bad_prediction) > 0:
        _thread.start_new_thread(hp.logging, (ct.low_certaincy_log_file, bad_prediction))
    
    print('result: ', categories)

    response = jsonify({'result' : categories})
    response.status_code = 200
    
    return response


if __name__ == '__main__':
    app.run(host= '0.0.0.0') 
