# Websosanh Product Classification System

## System overview

Our system consists of three individual product classifiers. The final result
is returned by a simple voting scheme. A detailed system overview can be found
[here](https://tinyurl.com/ydb8mdmu) (in Vietnamese).

